# Load a profile at login with Flatpak version of OpenRGB

If you are using OpenRGB as a Flatpak, you've probably seen that you can't load a profile at login because of the Flatpak sandbox.

This is a simple guide on how to make it work.


Note: This guide will show the terminal-way of fixing that, but the steps can be done using GUI apps if wanted.

## Get the .desktop file

To get the `.desktop` file that load your profile, open your terminal, and go to the flatpack autostart folder:

```
cd ~/.var/app/org.openrgb.OpenRGB/config/autostart
```

After running `ls`, you should see

```
OpenRGB.desktop
```

If not, you shouldn't have checked the auto load profile option.

You can see what it contains using a file editor (e.g. `nano`) or just `cat` it:

```
cat OpenRGB.desktop
```

And the output should look like something like this:

```
[Desktop Entry]
Categories=Utility;
Comment=OpenRGB 0.9, for controlling RGB lighting.
Icon=OpenRGB
Name=OpenRGB
StartupNotify=true
Terminal=false
Type=Application
Exec=/app/bin/openrgb --profile "<profile_name>"
```

## Create the autostarted .desktop file

And copy the default desktop entry to our autostart folder:
```
cp ~/.var/app/org.openrgb.OpenRGB/config/autostart/OpenRGB.desktop ~/.config/autostart/
```

Edit the file using any file editor (here with `nano`):

```
nano OpenRGB.desktop
```

Now, you only look at the last line of the file. Delete `/app/bin/openrgb` and replace it by the following text and **don't forget to keep the OpenRGB arguments**:

```
/usr/bin/flatpak run org.openrgb.OpenRGB
```

The last line should look like something like this:

```
Exec=/usr/bin/flatpak run org.openrgb.OpenRGB --profile "<profile_name>"
```

Use `openrgb --help` to see all available options you may want to add.

If not, look back to the last instruction.

Now save the file with `CTRL` + `O`, and quit `nano` with `CTRL` + `W`.

## Changing the auto loaded profile

First change it in OpenRGB's settings, and execute [part two](#create-the-autostarted-desktop-file) instructions.

## The end

From now, every time you login in your computer, the selected profile will be set automatically.
