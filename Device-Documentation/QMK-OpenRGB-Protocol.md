The QMK OpenRGB Protocol is an unofficial USB protocol for controlling QMK Firmware powered keyboards from OpenRGB using the Raw HID interface.  The protocol has been submitted upstream, but the QMK developers want to create a new unified QMK protocol, hence this one is unofficial.  Due to it being unofficial, you must enable it in the configuration file by adding the following section, adjusting it to add your keyboard's VID and PID if it is not in the list already.  The keyboards already in the list have been tested to work with the protocol.

You must flash your keyboard with a QMK Firmware build that has the protocol patches applied to use this.

```
    "QMKOpenRGBDevices": {
        "devices": [
            {
                "name": "Massdrop CTRL",
                "usb_pid": "EED2",
                "usb_vid": "04D8"
            },
            {
                "name": "Massdrop ALT",
                "usb_pid": "EED3",
                "usb_vid": "04D8"
            },
            {
                "name": "SonixQMK 0C45:5004",
                "usb_pid": "5004",
                "usb_vid": "0C45"
            },
            {
                "name": "SonixQMK 0C45:5104",
                "usb_pid": "5104",
                "usb_vid": "0C45"
            },
            {
                "name": "SonixQMK FEED:4B43",
                "usb_pid": "4B43",
                "usb_vid": "FEED"
            },
            {
                "name": "SonixQMK 0C45:652F",
                "usb_pid": "652F",
                "usb_vid": "0C45"
            },
            {
                "name": "KBDFans K67 MKII RGB",
                "usb_pid": "1225",
                "usb_vid": "4B42"
            }
        ]
    },
```